package jp.alhinc.watanabe_kento.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Copy {

	public static void main(String[] args) throws IOException {

		HashMap<String, String> branchNames = new HashMap<>();
		HashMap<String, Long> branchSales = new HashMap<>();

		File file = new File(args[0], "branch.lst");
		if (!file.exists()) {
			System.out.println("支店定義ファイルが存在しません。"); //ファイルが存在しない場合のエラー
			return;
		}

		FileReader fr = new FileReader(file); //フォルダから支店定義ファイルbranch.lstを読み込み。
		BufferedReader br = new BufferedReader(fr); //バッファにfrを投入
		try {
			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(","); /*itemsの値０には支店番号・１には店番号が読み込まれてる。*/
				if (items.length != 2 || !(items[0].matches("^[0-9]{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です。");
					return;
				}
				branchNames.put(items[0], items[1]); //branchNamesに格納。

			}
		} catch (IOException e) {
			System.out.println(e);
			br.close();
			return;
		} finally {
			br.close();
		}

		File[] files = new File(args[0]).listFiles(); //args[0]のパス内のファイル読み込み
		List<File> rcdFiles = new ArrayList<File>(); //売上フォルダの格納先。File型のファイルパスが入ってる。

		for (int i = 0; i < files.length; i++) { //ファイル名取得
			files[i].getName();
		}
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().matches("^[0-9]{8}.rcd$")) { //売上ファイルに該当するものを格納
				rcdFiles.add(files[i]);
			}
		}

		//2-2処理を実行。
		long saleAmount = (long) 0;
		for (int j = 0; j < rcdFiles.size(); j++) {
			FileReader fr2 = new FileReader(rcdFiles.get(j)); //引数はFile型。コマンドライン引数のパスからファイルを読み込み。
			BufferedReader br2 = new BufferedReader(fr2);
			try {
				String line2 = br2.readLine();
				long fileSale = Long.parseLong(br2.readLine()); //各売上の読み込み
				saleAmount = fileSale;
				if (branchSales.containsKey(line2)) {
					saleAmount = branchSales.get(line2) + fileSale;
				}
				System.out.println(saleAmount);
				branchSales.put(line2, saleAmount); //各支店の売上合計をbranchSalesに入れてる。;
				
			} catch (IOException e) {
				System.out.println(e);
				br2.close();
			} finally {
				br2.close();

			}
		}
		
		System.out.println("全支店と支店名，支店コード，合計金額を出力します。");
		File file2 = new File(args[0], "branch.out");
		FileWriter fw = new FileWriter(file2);
		BufferedWriter bw = new BufferedWriter(fw);
		try {
			for (String key : branchSales.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}
			bw.close();//branch.outに書き込み成功。
		} catch (IOException e) {
			System.out.println(e);
			bw.close();
		} finally {
			bw.close();
		}
	}
}