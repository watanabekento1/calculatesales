package jp.alhinc.watanabe_kento.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {

	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");//コマンドライン引数が１行であるか確認。
			return;
		}

		HashMap<String, String> branchNames = new HashMap<>();
		HashMap<String, Long> branchSales = new HashMap<>();
		HashMap<String, String> commodityNames = new HashMap<>();
		HashMap<String, Long> commoditySales = new HashMap<>(); //商品コードと売上額を格納

		if (!readFile(args[0], "branch.lst", "支店", "^[0-9]{3}$", branchNames, branchSales)) {
			return;
		}
		if (!readFile(args[0], "commodity.lst", "商品", "^[A-Za-z0-9]{8}$", commodityNames, commoditySales)) {
			return;
		}

		File[] saleFiles = new File(args[0]).listFiles(); //args[0]のパス内のファイル読み込み
		List<File> rcdFiles = new ArrayList<File>(); //売上ファイルの格納先。File型のファイルパスが入ってる。

		for (int i = 0; i < saleFiles.length; i++) {
			if ((saleFiles[i].getName().matches("^[0-9]{8}.rcd$")) && saleFiles[i].isFile()) {
				//売上ファイルが数字8文字且つファイルであるか確認。
				rcdFiles.add(saleFiles[i]);
			}
		}
		//並べ替え
		Collections.sort(rcdFiles);
		for (int j = 0; j < rcdFiles.size() - 1; j++) {
			int former = Integer.parseInt(rcdFiles.get(j).getName().substring(0, 8)); //２つのファイル名の先頭８文字の切り出し。
			int latter = Integer.parseInt(rcdFiles.get(j + 1).getName().substring(0, 8));
			//連番エラー処理
			if (latter - former != 1) {
				System.out.println("売上ファイルが連番になっていません");
				return;
			}
		}

		long branchSaleAmount = (long) 0; //支店別売上
		long commoditySaleAmount = (long) 0; //商品別売上額
		for (int i = 0; i < rcdFiles.size(); i++) {
			FileReader rcdFileReader = null;
			BufferedReader rcdBufferedReader = null;

			List<String> branchSalesFileData = new ArrayList<String>(); //売上ファイルの内容を格納するリストを作成。for文中で毎回初期化。

			try {
				rcdFileReader = new FileReader(rcdFiles.get(i)); //引数はFile型。コマンドライン引数のパスからファイルを読み込み。
				rcdBufferedReader = new BufferedReader(rcdFileReader);
				String saleData;
				while ((saleData = rcdBufferedReader.readLine()) != null) {//rcdFilesの各売り上げファイルのデータを１行ずつ全て読み込む。
					branchSalesFileData.add(saleData);
				}
				String branchCode = branchSalesFileData.get(0); //支店コード
				String commodityCode = branchSalesFileData.get(1); //商品コード
				String branchSale = branchSalesFileData.get(2); //売上額

				if (branchSalesFileData.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です"); //売り上げファイルのフォーマット確認。
					return;
				}

				if (!branchNames.containsKey(branchCode = branchSalesFileData.get(0))) {
					//売上ファイルから取得したkeyが支店定義ファイルに存在するかcontainsメソッドで確認。
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}
				if (!commodityNames.containsKey(commodityCode = branchSalesFileData.get(1))) {//売上ファイルの商品コード確認
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
				}
				if (!branchSalesFileData.get(2).matches("^[0-9]\\d*$")) {//売上額が数字か確認。
					System.out.println("予期せぬエラーが生じました");
					return;
				}

				branchSaleAmount = Long.parseLong(branchSale);
				if (branchSales.containsKey(branchCode)) {
					branchSaleAmount = branchSales.get(branchCode)
							+ Long.parseLong(branchSale);
				}

				commoditySaleAmount = Long.parseLong(branchSale); //商品ごとの累計売上額を算出。
				if (commodityNames.containsKey(commodityCode)) {
					commoditySaleAmount = commoditySales.get(commodityCode) + Long.parseLong(branchSale);
				}

				if (branchSaleAmount >= 10000000000L) {
					System.out.println("合計金額が合計10桁を超えました"); //売上桁数エラー
					return;
				}
				if (commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が合計10桁を超えました"); //売上桁数エラー
					return;
				}

				branchSales.put(branchSalesFileData.get(0), branchSaleAmount); //各支店の売上合計をbranchSalesに入れてる。;
				commoditySales.put(branchSalesFileData.get(1), commoditySaleAmount);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (rcdBufferedReader != null) {
					try {

						rcdBufferedReader.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		if (!outPutFile(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

		if (!outPutFile(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}

	private static boolean readFile(String filePath, String fileName, String dataType, String conditions,
			HashMap<String, String> namesMap,HashMap<String, Long> salesMap) {
		File file = new File(filePath, fileName );
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		if (!file.exists()) {
			System.out.println(dataType+"定義ファイルが存在しません");
			return false;
		}
		try {
			fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);

			String fileLine;

			while ((fileLine = bufferedReader.readLine()) != null) {
				String[] namesData = fileLine.split(",");
				//支店コード桁数チェック
				if (namesData.length != 2 || !(namesData[0].matches(conditions))) {

					System.out.println(dataType + "定義ファイルのフォーマットが不正です");
					return false;
				}

				namesMap.put(namesData[0], namesData[1]);
				salesMap.put(namesData[0], (long) 0);

			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	private static boolean outPutFile(String filePath, String fileName, HashMap<String, String> namesMap,
			HashMap<String, Long> salesMap) {
		File allSales = new File(filePath, fileName);
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {
			fileWriter = new FileWriter(allSales);
			bufferedWriter = new BufferedWriter(fileWriter);

			for (String key : namesMap.keySet()) {

				bufferedWriter.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				bufferedWriter.newLine();

			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bufferedWriter != null) {
				try {
					bufferedWriter.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}